import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default function App() {
  return (
    <>
      <View style={styles.cont}>
        <View style={styles.bg} >
          <Image
            style={styles.banner}
            source={require("./assets/img/bg.jpg")}
          />
        </View>

        <ScrollView>

          <View style={{ marginHorizontal: 10 }}>
            <Text style={styles.title}>Qué hacer en Paris</Text>
            <ScrollView
              horizontal
            >
              <View>
                <Image
                  style={styles.country}
                  source={require("./assets/img/actividad1.jpg")}
                />
              </View>
              <View>
                <Image
                  style={styles.country}
                  source={require("./assets/img/actividad2.jpg")}
                />
              </View>
              <View>
                <Image
                  style={styles.country}
                  source={require("./assets/img/actividad3.jpg")}
                />
              </View>
              <View>
                <Image
                  style={styles.country}
                  source={require("./assets/img/actividad4.jpg")}
                />
              </View>
              <View>
                <Image
                  style={styles.country}
                  source={require("./assets/img/actividad5.jpg")}
                />
              </View>
            </ScrollView>

            <Text style={styles.title}>Mejores Alojamientos</Text>

            <View>
              <View>
                <Image
                  style={styles.mejores}
                  source={require("./assets/img/mejores1.jpg")}
                />
              </View>
              <View>
                <Image
                  style={styles.mejores}
                  source={require("./assets/img/mejores2.jpg")}
                />
              </View>
              <View>
                <Image
                  style={styles.mejores}
                  source={require("./assets/img/mejores3.jpg")}
                />
              </View>
            </View>

            <Text style={styles.title}>Mejores Hospedajes</Text>

            <View style={ styles.listado }>
              <View style={ styles.listadoItem}>
                <Image
                  style={styles.mejores}
                  source={require("./assets/img/hospedaje1.jpg")}
                />
              </View>
              <View style={ styles.listadoItem}>
                <Image
                  style={styles.mejores}
                  source={require("./assets/img/hospedaje2.jpg")}
                />
              </View>
              <View style={ styles.listadoItem}>
                <Image
                  style={styles.mejores}
                  source={require("./assets/img/hospedaje3.jpg")}
                />
              </View>
              <View style={ styles.listadoItem}>
                <Image
                  style={styles.mejores}
                  source={require("./assets/img/hospedaje4.jpg")}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  cont: {
    flex: 1,
    backgroundColor: "#A8D8F7"
  },
  banner: {
    height: 200,
    flex: 1
  },
  bg: {
    flexDirection: 'row'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    marginVertical: 10
  },
  country: {
    width: 200,
    height: 250,
    marginRight: 5
  },
  mejores: {
    width: '100%',
    height: 200,
    marginBottom: 5
  },
  listado: {
    flexDirection:'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
  },
  listadoItem: {
    flexBasis: '49%'
  }
});
